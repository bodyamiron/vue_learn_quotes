import Vue from 'vue'
import App from './App.vue'

const a = new Vue({
  el: '#app',
  render: h => h(App)
})

console.log(a);
